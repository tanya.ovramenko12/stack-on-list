﻿#include <string>
#include <iostream>

class Stack
{
public:
    Stack() //конструктор без параметров
    {
        size = 0;
        head = nullptr;
    }
    void push(int val) // добавление элемента в конец
    {
        Node* node = new Node();
        node->val = val;
        node->next = nullptr;
        if (size == 0)
        {
            head = node;
            size++;
            return;
        }
        Node* current = head;
        while (current->next != nullptr)
        {
            current = current->next;
        }
        current->next = node;
        size++;
    }
    void pop() // уадление элемента с конца
    {
        if (size == 0)
        {
            throw std::exception("Stack is empty!");
        }
        if (size == 1)
        {
            size--;
            delete head;
            head = nullptr;
            return;
        }
        Node* current = head;
        while (current->next->next != nullptr)
        {
            current = current->next;
        }
        delete current->next;
        current->next = nullptr;
        size--;
    }
    int front_el()  // возвращает верхний элемент 
    {
        if (size == 0)
        {
            throw std::exception("Stack is empty!");
        }
        Node* current = head;
        while (current->next != nullptr)
        {
            current = current->next;
        }
        return current->val;
    }
    bool is_empty() // проверяет стек на пустоту
    {
        if (size == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void print() // вывод стека
    {
        if (size == 0)
        {
            throw std::exception("Stack is empty!");
        }
        Node* current = head;
        while (current->next != nullptr)
        {
            std::cout << current->val << " ";
            current = current->next;
        }
        std::cout << current->val << std::endl;
    }
private:
    struct Node
    {
        int val;
        Node* next;
    };
    int size;
    Node* head;
};

int main()
{
    std::cout << "Stack on List" << std::endl << "1. add value" << std::endl << "2. delete value" <<
        std::endl << "3. front element" << std::endl << "4. is stack empty" << std::endl << "5. print stack" << std::endl << "6. end programme"
        << std::endl;
    Stack* stack = new Stack();
    std::string str;
    do
    {
        getline(std::cin, str);
        if (str == "add value")
        {
            int val;
            std::cin >> val;
            stack->push(val);
        }
        else if (str == "delete value")
        {
            try
            {
                stack->pop();
            }
            catch (std::exception ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "front element")
        {
            try
            {
                stack->front_el();
            }
            catch (std::exception ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "is stack empty")
        {
            if (stack->is_empty())
            {
                std::cout << "Stack is empty." << std::endl;
            }
            else
            {
                std::cout << "Stack is not empty." << std::endl;
            }
        }
        else if (str == "print stack")
        {
            try
            {
                stack->print();
            }
            catch (std::exception ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
    } while (str != "end program");
    system("pause");
}